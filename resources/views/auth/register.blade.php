<div class="container container-signup container-transparent animated fadeInRight">
    <h3 class="text-center">Sign Up</h3>
    <form method="POST" action="{{ route('register') }}">
    @csrf
    <div class="login-form">
        <div class="form-group">
            <label for="name" class="placeholder"><b>Fullname</b></label>
            <input id="name" type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

            @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label for="email" class="placeholder"><b>Email</b></label>
            <input  id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label for="password" class="placeholder"><b>Password</b></label>
            <div class="position-relative">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                <div class="show-password">
                    <i class="icon-eye"></i>
                </div>
                
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label for="password-confirm" class="placeholder"><b>Confirm Password</b></label>
            <div class="position-relative">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                <div class="show-password">
                    <i class="icon-eye"></i>
                </div>
            </div>
        </div>
        <div class="row form-sub m-0">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" name="agree" id="agree">
                <label class="custom-control-label" for="agree">I Agree the terms and conditions.</label>
            </div>
        </div>
        <div class="row form-action">
            <div class="col-md-6">
                <a href="#" id="show-signin" class="btn btn-danger btn-link w-100 fw-bold">Cancel</a>
            </div>
            <div class="col-md-6">
                <button class="btn btn-primary w-100 fw-bold">Sign Up</button>
            </div>
        </div>
    </div>
    </form>
</div>