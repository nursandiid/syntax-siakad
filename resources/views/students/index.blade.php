@extends('layouts.master')

@section('content')
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Daftar Mahasiswa</h2>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <a href="{{ route('students.create') }}" class="btn btn-white btn-sm btn-border"><i class="fas fa-plus-circle"></i> Tambah</a>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-12">
            @card
                @slot('title')
                    
                @endslot

                @table
                    @slot('thead')
                        <th>No</th>
                        <th>NIM</th>
                        <th>Nama</th>
                        <th>Gender</th>
                        <th>Jurusan</th>
                        <th>Angkatan</th>
                        <th>Aksi</th>
                    @endslot
                    
                    @php $no = 1; @endphp
                    @foreach ($students as $student)
                        <tr>
                            <td width="5%">{{ $no++ }}.</td>
                            <td>{{ $student->nim }}</td>
                            <td width="25%">{{ $student->nama }}</td>
                            <td>{{ $student->jenis_kelamin == 'L' ? 'Laki-laki' : 'Perempuan' }}</td>
                            <td>
                                @foreach ($departments as $department)
                                    {{ $department->kode == $student->kode_jurusan ? $department->nama : '' }}
                                @endforeach
                            </td>
                            <td width="5%">{{ $student->angkatan }}</td>
                            <td width="5%">
                                <form method="POST" action="{{ route('students.destroy', $student->id) }}">
                                    @csrf @method('delete')
                                    <div class="form-button-action text-center">
                                        <a href="{{ route('students.edit', $student->id) }}" class="btn btn-link btn-primary" title="Edit"><i class="far fa-edit"></i></a>
                                        <button class="btn btn-link btn-danger" title="Delete" onclick="return confirm('Yakin?')"><i class="fas fa-trash-alt"></i></button>
                                        <a href="{{ route('students.show', $student->id) }}" class="btn btn-link btn-default" title="Detail"><i class="fas fa-search-plus"></i></a>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endtable
                <div class="card-footer mt-3">
                    <a onclick="window.open('{{ url('/students/print/ktm/') }}', 'Nota PDF', 'height=675, width=1024, left=175, scrollbars=yes');" class="btn btn-primary btn-sm text-white"><i class="fas fa-print"></i> Cetak KTM</a>
                </div>
            @endcard
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.table').DataTable({
        "language": {
            "zeroRecords": "<strong>Data tidak ditemukan</strong>"
        }
    })

    $('.card-header').addClass('d-none')
</script>
@endpush