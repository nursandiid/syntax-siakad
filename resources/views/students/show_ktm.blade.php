<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Cetak KTM</title>
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">

	<style>
		.card-body td {
			font-size: .8em;
		}
	</style>
</head>
<body onload="window.print()">
	<div class="container-fluid">
		<div class="row mt-3">
			<!-- Looping Here -->
			@foreach ($students as $student)
			<div class="col-6">
				<div class="card mb-3">
					<div class="card-header p-0" style="overflow: hidden;">
						 <img src="{{ asset('assets/img/kop.jpg') }}" alt="" width="100%">
					</div>

					<div class="row no-gutters">
						<div class="col-md-4">
							@if (file_exists(public_path().'/assets/img/uploads/'.$student->foto))
							<img src="{{ asset('assets/img/uploads/') }}/{{ $student->foto }}" alt="" class="card-img p-4">
							@else
							<img src="{{ asset('assets/img/uploads/') }}/default.png" alt="" class="card-img p-4">
							@endif
							<p class="text-center" style="font-size: .8em;"><strong>Berlaku: Selama Menjadi Mahasiswa</strong></p>
						</div>
						<div class="col-md-8">
							<div class="card-body">
								<h5 class="card-title font-weight-bold">KARTU TANDA MAHASISWA</h5>
								<table>
									<tr>
										<td width="35%">Nama</td>
										<td>: {{ $student->nama }}</td>
									</tr>
									<tr>
										<td>Tmpt/Tgl. Lahir</td>
										<td>: {{ $student->tempat_lahir }}, {{ $student->tanggal_lahir }}</td>
									</tr>
									<tr>
										<td>NIM</td>
										<td>: {{ $student->nim }}</td>
									</tr>
									<tr>
										<td>Tahun Masuk</td>
										<td>: {{ $student->angkatan }}</td>
									</tr>
									<tr>
										<td>Prodi</td>
										<td>: 
											@foreach ($departments as $department)
												{{ $department->kode == $student->kode_jurusan ? $department->nama : '' }}
											@endforeach
										</td>
									</tr>
									<tr>
										<td>Alamat</td>
										<td>: {{ $student->alamat }}</td>
									</tr>
									<tr>
										<td>Gol. Darah</td>
										<td>: -</td>
									</tr>
									<tr>
										<td>Telp/HP</td>
										<td>: {{ $student->telpon }}</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</body>
</html>