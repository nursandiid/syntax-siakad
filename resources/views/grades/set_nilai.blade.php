@extends('layouts.master')

@section('content')
<div class="page-inner mt-2">
    <div class="col-12">
        @card
            @slot('title')
                Rekap Nilai
            @endslot
            
            <form method="post" action="{{ route('grades.store') }}">
                @csrf
                <div class="form-group row">
                    <label for="nim" class="col-sm-2 col-form-label">NIM</label>
                    <div class="col-sm-4">
                        <input class="form-control" name="nim" id="nim" value="{{ $data->nim_mahasiswa }}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama_mahasiswa" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="nama_mahasiswa" name="nama_mahasiswa" value="{{ $data->nama_mahasiswa }}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="kode_mk" class="col-sm-2 col-form-label">Kode MK</label>
                    <div class="col-sm-4">
                        <input class="form-control" name="kode_mk" id="kode_mk" required value="{{ $data->kode_mk }}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama_mk" class="col-sm-2 col-form-label">Nama Mata Kuliah</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="nama_mk" name="nama_mk" required value="{{ $data->nama_mk }}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sks" class="col-sm-2 col-form-label">SKS</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="sks" name="sks" required value="{{ $data->sks }}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="kode_dosen" class="col-sm-2 col-form-label">Kode Dosen</label>
                    <div class="col-sm-2">
                        <input class="form-control" name="kode_dosen" id="kode_dosen" required value="{{ $data->kode_dosen }}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama_dosen" class="col-sm-2 col-form-label">Nama Dosen</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="nama_dosen" name="nama_dosen" required value="{{ $data->nama_dosen }}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="semester" class="col-sm-2 col-form-label">Semester</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="semester" name="semester" required value="{{ $data->semester }}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="tugas" class="col-sm-2 col-form-label">Tugas</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="tugas" id="tugas" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="uts" class="col-sm-2 col-form-label">UTS</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="uts" id="uts" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="uas" class="col-sm-2 col-form-label">UAS</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="uas" id="uas" required>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-sm-6 offset-sm-3">
                            <button class="btn btn-primary"> <i class="far fa-save"></i> Simpan</button>
                            <button class="btn btn-danger ml-3" onclick="self.history.back()"><i class="fas fa-arrow-circle-left"></i> Kembali</button>
                        </div>                                      
                    </div>
                </div>
            </form>
        @endcard
    </div>
</div>
@endsection

@push('scripts')
<script>

    $('label.col-sm-2').addClass('offset-sm-1')
    $('label.offset-sm-1').append(` <span class="required-label">*</span>`)
    $('.form-group .form-control').addClass('form-control-sm')
</script>
@endpush