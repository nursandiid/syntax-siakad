@extends('layouts.master')

@section('content')
<div class="page-inner mt-2">
    <div class="col-12">
        @card
            @slot('title')
                Input Nilai Mahasiswa
            @endslot

            @table
                @slot('thead')
                    <th>NO</th>
                    <th>NIM</th>
                    <th>Nama</th>
                    <th>Semester</th>
                    <th>Jurusan</th>
                    <th>Matkul</th>
                    <th>Aksi</th>
                @endslot
                
                @php $no  = 1; @endphp
                @foreach ($students as $student)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $student->nim }}</td>
                        <td>{{ $student->nama_mahasiswa }}</td>
                        <td>{{ $student->semester }}</td>
                        <td>{{ $student->nama_jurusan }}</td>
                        <td>{{ $student->nama_mk }}</td>
                        <td>
                            <div class="form-button-action text-center">
                                <button class="btn btn-sm btn-default" disabled style="cursor: not-allowed;"><i class="far fa-edit"></i> Masukan Nilai</button>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @endtable
        @endcard

        @card
            @slot('title')
                Rekap Nilai
            @endslot
            
            <form method="post">
                @csrf @method('put')
                <div class="form-group row">
                    <label for="matkul" class="col-sm-2 col-form-label">Mata Kuliah</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="matkul" id="matkul" readonly value="{{ $course->nama_mk }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sks" class="col-sm-2 col-form-label">SKS</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="sks" id="sks" readonly value="{{ $course->sks }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="nama_dosen" class="col-sm-2 col-form-label">Nama Dosen</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="nama_dosen" id="nama_dosen" readonly value="{{ $course->nama_dosen }}">
                    </div>
                </div>
                
                <table class="table table-bordered table1 mt-3">
                    <thead>
                        <tr>
                            <th rowspan="2" class="text-center">No</th>
                            <th rowspan="2" class="text-center">Nim</th>
                            <th rowspan="2" class="text-center">Nama</th>
                            <th colspan="5" class="text-center">Nilai</th>
                            <th rowspan="2" class="text-center">TP</th>
                            <th colspan="2" class="text-center">Nilai Akhir</th>
                            <th rowspan="2" class="text-center">Aksi</th>
                        </tr>
                        @if (count($grades) != null)
                        <tr>
                            <th class="text-center">Presensi ({{ $grades[0]->prepre * 100 }}%)</th>
                            <th class="text-center">Tugas ({{ $grades[0]->pretug * 100 }}%)</th>
                            <th class="text-center">Quiz ({{ $grades[0]->prequi * 100 }}%)</th>
                            <th class="text-center">UTS ({{ $grades[0]->preuts * 100 }}%)</th>
                            <th class="text-center">UAS ({{ $grades[0]->preuas * 100 }}%)</th>
                            <th class="text-center">HM</th>
                            <th class="text-center">AM</th>
                        </tr>
                        @else
                            <tr>
                                <th class="text-center">Presensi (0)</th>
                                <th class="text-center">Tugas (0)</th>
                                <th class="text-center">Quiz (0)</th>
                                <th class="text-center">UTS (0)</th>
                                <th class="text-center">UAS (0)</th>
                                <th class="text-center">HM</th>
                                <th class="text-center">AM</th>
                            </tr>
                        @endif
                    </thead>
                    <tbody>
                        @php $no   = 1; @endphp
                        @if (count($grades) != null)
                        @foreach ($grades as $grade)
                            <!-- Logic PHP -->
                            @php  
                            if ($grade->presensi != 0) 
                                $total = ($grade->presensi * 100 / $grade->presensi * $grade->prepre) + ($grade->tugas * $grade->pretug) + ($grade->quiz * $grade->prequi) + ($grade->uts * $grade->preuts) + ($grade->uas * $grade->preuas);
                            else 
                                $total  = ($grade->presensi * $grade->prepre) + ($grade->tugas * $grade->pretug) + ($grade->quiz * $grade->prequi) + ($grade->uts * $grade->preuts) + ($grade->uas * $grade->preuas);

                            if ($total >= 85 || $total <= 99) $grade_ = "A";
                            if ($total <= 80 || $total <= 84) $grade_ = "A-";
                            if ($total <= 75 || $total <= 79) $grade_ = "B+";
                            if ($total <= 70 || $total <= 74) $grade_ = "B";
                            if ($total <= 65 || $total <= 69) $grade_ = "B-";
                            if ($total <= 60 || $total <= 64) $grade_ = "C+";
                            if ($total <= 55 || $total <= 59) $grade_ = "C";
                            if ($total <= 55 || $total <= 54) $grade_ = "C-";
                            if ($total <= 40 || $total <= 49) $grade_ = "D";

                            if ($grade_     == "A")  $mutu = 4.00;
                            elseif ($grade_ == "A-") $mutu = 3.70;
                            elseif ($grade_ == "B+") $mutu = 3.30;
                            elseif ($grade_ == "B")  $mutu = 3.00;
                            elseif ($grade_ == "B-") $mutu = 2.70;
                            elseif ($grade_ == "C+") $mutu = 2.30;
                            elseif ($grade_ == "C")  $mutu = 2.00;
                            elseif ($grade_ == "C-") $mutu = 1.70;
                            elseif ($grade_ == "D")  $mutu = 1.00;
                            else $mutu     = 0.00;
                            
                            $mutu_hasil = $grade->sks * $mutu;
                            @endphp
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $grade->nim }}</td>
                                <td>{{ $grade->nama_mahasiswa }}</td>
                                <td class="text-center">{{ $grade->presensi }}</td>
                                <td class="text-center">{{ $grade->tugas }}</td>
                                <td class="text-center">{{ $grade->quiz }}</td>
                                <td class="text-center">{{ $grade->uts }}</td>
                                <td class="text-center">{{ $grade->uas }}</td>        
                                <td class="text-center">{{ $total }}</td>
                                <td class="text-center">{{ $grade_ }}</td>
                                <td class="text-center">{{ number_format($mutu, 2) }}</td>
                                <td>
                                    <button class="btn btn-sm btn-primary" disabled style="cursor: not-allowed;">Edit Nilai (%)</button>
                                </td>
                            </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="12" class="fw-bold text-center">Ups!! Data Tidak ditemukan.</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-sm-6">
                        <button class="btn btn-primary btn-sm"> <i class="fas fa-print"></i> Cetak Rekap Nilai</button>
                        <a href="{{ url('/grades') }}" class="btn btn-danger btn-sm ml-3"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
                    </div>                                      
                </div>
            </form>
        @endcard
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.table:not(.table1)').DataTable({
        "language": {
            "zeroRecords": "<strong>Data tidak ditemukan</strong>"
        }
    })
    
    $('label.offset-sm-1').append(` <span class="required-label">*</span>`)
    $('.form-group .form-control').addClass('form-control-sm')
</script>
@endpush