<!-- Sidebar -->
<div class="sidebar sidebar-style-2">
	<div class="sidebar-wrapper scrollbar scrollbar-inner">
		<div class="sidebar-content">
			<div class="user">
				<div class="avatar-sm float-left mr-2">
					@if (file_exists(public_path().'/assets/img/uploads/'.Auth::user()->image))
					<img src="{{ asset('/assets/img/uploads/') }}/{{ Auth::user()->image }}" alt="..." class="avatar-img rounded-circle">
					@else
					<img src="{{ asset('assets/img/uploads/') }}/default.png" class="avatar-img rounded-circle">
					@endif
				</div>
				<div class="info">
					<a href="{{ url('/users/profile') }}">
						<span>
							{{ ucfirst(Auth::user()->name) }}
							<span class="user-level">
								@if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
									Administrator
								@elseif(Auth::user()->role_id == 3)
									Mahasiswa
								@else
									Dosen
								@endif

							</span>
						</span>
					</a>
				</div>
			</div>
			<ul class="nav nav-primary">
				<li class="nav-item">
					<a href="{{ url('/') }}">
						<i class="fas fa-home"></i>
						<p>Dashboard</p>
					</a>
				</li>
				<li class="nav-section">
					<span class="sidebar-mini-icon">
						<i class="fa fa-ellipsis-h"></i>
					</span>
					<h4 class="text-section">MAIN NAVIGATION</h4>
				</li>
				@if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
				
				<li class="nav-item">
					<a href="{{ url('/students') }}">
						<i class="fas fa-user-graduate"></i>
						<p>Mahasiswa</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ url('/curriculums') }}">
						<i class="far fa-newspaper"></i>
						<p>Kurikulum</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ url('/lecturers') }}">
						<i class="fas fa-users"></i>
						<p>Dosen</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ url('/departments') }}">
						<i class="fas fa-chalkboard"></i>
						<p>Jurusan</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ url('/courses') }}">
						<i class="fas fa-book"></i>
						<p>Mata Kuliah</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ url('/rooms') }}">
						<i class="fas fa-university"></i>
						<p>Ruangan</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ url('/schedules') }}">
						<i class="fas fa-clipboard-list"></i>
						<p>Jadwal</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ url('/grades') }}">
						<i class="flaticon-interface-6"></i>
						<p>Nilai Mahasiswa</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ url('/students/certificates') }}">
						<i class="fas fa-th-large"></i>
						<p>Ijazah</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ url('/years') }}">
						<i class="fas fa-calendar-alt"></i>
						<p>Tahun Akademik</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ url('/users') }}">
						<i class="fas fa-users-cog"></i>
						<p>Manajemen User</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ url('/settings') }}">
						<i class="fas fa-cogs"></i>
						<p>Pengaturan</p>
					</a>
				</li>
				@elseif(Auth::user()->role_id == 3)
				
				<li class="nav-item">
					<a data-toggle="collapse" href="#profile" class="collapsed profile" aria-expanded="false">
						<i class="fas fa-user-graduate"></i>
						<p>Profile</p>
						<span class="caret"></span>
					</a>
				</li>
				<div class="collapse" id="profile">
					<ul class="nav nav-collapse">
						<li class="nav-item">
							<a href="{{ url('/user/student/profile') }}">
								<i class="fas fa-user"></i>
								<p>Profile</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('/user/student/profile/edit') }}">
								<i class="fas fa-user-edit"></i>
								<p>Edit Profile</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('/user/student/password/edit') }}">
								<i class="fas fa-key"></i>
								<p>Edit Password</p>
							</a>
						</li>
					</ul>
				</div>
				<li class="nav-item">
					<a href="{{ url('/user/student/krs') }}">
						<i class="fas fa-th-large"></i>
						<p>KRS</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ url('/user/student/khs') }}">
						<i class="fas fa-id-card-alt"></i>
						<p>KHS</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ url('/user/student/transkip') }}">
						<i class="fas fa-layer-group"></i>
						<p>Transkip Nilai</p>
					</a>
				</li>
				@elseif(Auth::user()->role_id == 4)
				<li class="nav-item">
					<a data-toggle="collapse" href="#profile" class="collapsed profile" aria-expanded="false">
						<i class="fas fa-user"></i>
						<p>Profile</p>
						<span class="caret"></span>
					</a>
				</li>
				<div class="collapse" id="profile">
					<ul class="nav nav-collapse">
						<li class="nav-item">
							<a href="{{ url('/user/lecturer/profile') }}">
								<i class="fas fa-user"></i>
								<p>Profile</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('/user/lecturer/profile/edit') }}">
								<i class="fas fa-user-edit"></i>
								<p>Edit Profile</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('/user/lecturer/password/edit') }}">
								<i class="fas fa-key"></i>
								<p>Edit Password</p>
							</a>
						</li>
					</ul>
				</div>
				<li class="nav-item">
					<a href="{{ url('/schedules') }}">
						<i class="fas fa-clipboard-list"></i>
						<p>Jadwal</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ url('/grades') }}">
						<i class="flaticon-interface-6"></i>
						<p>Nilai Mahasiswa</p>
					</a>
				</li>
				@endif
			</ul>
		</div>
	</div>
</div>