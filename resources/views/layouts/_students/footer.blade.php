<footer class="footer">
	<div class="container">
		<nav class="pull-left">
			<ul class="nav">
				<strong>Copyright &copy; 2019</strong> All rights reserved, Designed by <a href="https://www.themekita.com">ThemeKita</a>
			</ul>
		</nav>
		<div class="copyright ml-auto">
            Developed with <i class="fa fa-heart heart text-danger"></i> by <a href="https://www.nursandi.com">Nursandi</a>
        </div>
	</div>
</footer>