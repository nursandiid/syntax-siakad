<div class="main-header" data-background-color="blue">
    
    <div class="nav-top">
        <div class="container d-flex flex-row">
            <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse"
            data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon">
                    <i class="icon-menu"></i>
                </span>
            </button>
            <button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
            <!-- Logo Header -->
            <a href="{{ url('/') }}" class="logo d-flex align-items-center">
                <img src="{{ asset('/assets/img/logo.png') }}" alt="navbar brand" class="navbar-brand" width="120">
            </a>
            <!-- End Logo Header -->
            <!-- Navbar Header -->
            <nav class="navbar navbar-header navbar-expand-lg p-0">
                <div class="container-fluid p-0">
                    <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
                        <li class="nav-item dropdown hidden-caret">
                            <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"
                                aria-expanded="false">
                                <div class="avatar-sm">
                                    @if (file_exists(public_path().'/assets/img/uploads/'.Auth::user()->image))
                                    <img src="{{ asset('/assets/img/uploads/') }}/{{ Auth::user()->image }}" alt="..." class="avatar-img rounded-circle">
                                    @else
                                    <img src="{{ asset('/assets/img/uploads/') }}/default.png" alt="..." class="avatar-img rounded-circle">
                                    @endif
                                </div>
                            </a>
                        </li>
                        <li class="nav-item dropdown hidden-caret">
                            <a href="#" class="fw-bold text-white" style="cursor: not-allowed; font-size: 1em;">{{ ucfirst(Auth::user()->name) }}</a>
                        </li>
                        <li class="nav-item dropdown hidden-caret">
                            <a class="ml-3 btn border btn-sm fw-bold text-white" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" title="Logout">
                                <i class="link-icon fa fa-sign-out-alt"></i>
                                <span class="menu-title">Logout</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- End Navbar -->
        </div>
    </div>
    <div class="container-fluid">
        <div class="nav-bottom bg-white rounded">
            <ul class="nav page-navigation page-navigation-primary justify-content-center">
                <li class="nav-item submenu">
                    <a class="nav-link" href="{{ url('/') }}">
                        <i class="link-icon fas fa-home text-muted"></i>
                        <span class="menu-title">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item submenu">
                    <a class="nav-link profile" href="#">
                        <i class="link-icon text-muted fas fa-user-graduate"></i>
                        <span class="menu-title">Profile</span>
                    </a>
                    <div class="navbar-dropdown animated fadeIn">
                        <ul>
                            <li>
                                <a href="{{ url('/user/student/profile') }}">Profile</a>
                            </li>
                            <li>
                                <a href="{{ url('/user/student/profile/edit') }}">Edit Profile</a>
                            </li>
                            <li>
                                <a href="{{ url('/user/student/password/edit') }}">Edit Password</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item submenu">
                    <a class="nav-link" href="{{ url('/user/student/biodata') }}">
                        <i class="link-icon text-muted fas fa-id-card-alt"></i>
                        <span class="menu-title">Biodata</span>
                    </a>
                </li>
                <li class="nav-item submenu">
                    <a class="nav-link" href="{{ url('/user/student/address') }}">
                        <i class="link-icon text-muted fas fa-map-marker-alt"></i>
                        <span class="menu-title">Alamat</span>
                    </a>
                </li>
                <li class="nav-item submenu">
                    <a class="nav-link" href="{{ url('/user/student/family') }}">
                        <i class="link-icon text-muted fas fa-users"></i>
                        <span class="menu-title">Keluarga</span>
                    </a>
                </li>
                <li class="nav-item submenu">
                    <a class="nav-link" href="{{ url('/user/student/krs') }}">
                        <i class="link-icon text-muted fas fa-th-large"></i>
                        <span class="menu-title">KRS</span>
                    </a>
                </li>
                <li class="nav-item submenu">
                    <a class="nav-link" href="{{ url('/user/student/khs') }}">
                        <i class="link-icon text-muted fas fa-th-large"></i>
                        <span class="menu-title">KHS</span>
                    </a>
                </li>
                <li class="nav-item submenu">
                    <a class="nav-link" href="{{ url('/user/student/uploads') }}" title="Upload & Download Lampiran">
                        <i class="link-icon text-muted fas fa-download"></i>
                        <span class="flaticon-mark text-muted"></span>
                    </a>
                </li>
                <li class="nav-item submenu">
                    <a class="nav-link" href="{{ url('/user/student/transkip') }}">
                        <i class="link-icon text-muted fas fa-layer-group"></i>
                        <span class="menu-title">Transkip Nilai</span>
                    </a>
                </li>
                <li class="nav-item submenu d-none">
                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" title="Logout">
                        <i class="link-icon text-muted fa fa-sign-out-alt"></i>
                        <span class="menu-title">Logout</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none">@csrf</form>
                </li>
              </a>
            </ul>
        </div>
    </div>
</div>