@extends('layouts._students.master')

@section('content')
<div class="row mt-2 d-lg-block d-none"></div>
<div class="page-inner">
    <div class="row">
        <div class="col-sm-6 col-lg-3">
            <div class="card p-3">
                <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-secondary mr-3">
                        <i class="fa fa-dollar-sign"></i>
                    </span>
                    <div>
                        <h5 class="mb-1"><b><a href="#">132 <small>Sales</small></a></b></h5>
                        <small class="text-muted">12 waiting payments</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    
</script>
@endpush