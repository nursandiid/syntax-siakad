@extends('layouts._students.master')

@push('css')
    <style>
        .table tr:last-child td{
            
            border-top: 1px solid #a0a0a0;
        }
    </style>
@endpush

@section('content')
<div class="page-inner mt-2">
    <div class="col-12">
        <form method="post">
        @csrf
        @card
            @slot('title')
            <strong>Kartu Rencana Studi Yang Telah Diambil</strong>
            @endslot
            

            <div class="form-group row">
                <label for="nim" class="col-sm-2 col-form-label">NIM</label>
                <div class="col-sm-4">
                    <input class="form-control" id="nim" name="nim" value="{{ $student->nim }}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                <div class="col-sm-6">
                    <input class="form-control" id="nama" name="nama" value="{{ $student->nama_mahasiswa }}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="kode_jurusan" class="col-sm-2 col-form-label">Jurusan</label>
                <div class="col-sm-4">
                    <input class="form-control" value="{{ $student->nama_jurusan }}" readonly>
                    <input class="form-control d-none" id="kode_jurusan" name="kode_jurusan" value="{{ $student->kode_jurusan }}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="kelas" class="col-sm-2 col-form-label">Kelas</label>
                <div class="col-sm-3">
                    <input class="form-control" id="kelas" name="kelas" value="{{ $student->nama_kelas }}" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label for="semester" class="col-sm-2 col-form-label">Semester Yang Ditempuh</label>
                <div class="col-sm-2">
                    <input class="form-control" id="semester" name="semester" value="{{ $student->semester }}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="tahun_akad" class="col-sm-2 col-form-label">tahun_akad</label>
                <div class="col-sm-4">
                    <input class="form-control" id="tahun_akad" name="tahun_akad" value="{{ $student->angkatan -1 }}/{{ $student->angkatan }}" readonly>
                </div>
            </div>

            <table class="table table-bordered">
                <thead>
                    <th>No</th>
                    <th>Kode Matkul</th>
                    <th>Nama Mata Kuliah</th>
                    <th>SKS</th>
                    <th>Dosen</th>
                    <th>Ruang</th>
                    <th>Hari</th>
                    <th>Jam</th> 
                    <th>Kelas</th>
                </thead>

                @php 
                    $no = 1; 
                    $sks = 0;
                @endphp
                <tbody>
                    @foreach ($courses as $course)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $course->kode_mk }}</td>
                        <td>{{ $course->nama_mk }}</td>
                        <td>{{ $course->sks }}</td>
                        <td>{{ $course->nama_dosen }}</td>
                        <td>{{ $course->nama_ruang }}</td>
                        <td>{{ $course->hari }}</td>
                        <td><span class="badge badge-dark rounded" style="font-size: .8em">{{ $course->jam_mulai }} - {{ $course->jam_selesai }} WIB</span></td>
                        <td>{{ $course->nama_kelas }}</td>

                        @php  
                        $sks += $course->sks;
                        @endphp
                    </tr>
                    @endforeach
                </tbody>
                <tr style="border-top: 1px solid #a0a0a0;">
                    <td></td>
                    <td colspan="2"><strong>Total SKS</strong></td>
                    <td><strong>{{ $sks }}</strong></td>
                    <td colspan="5"></td>
                </tr>
            </table>
            <div class="card-footer">
                <div class="row">
                    <div class="col-sm-6">
                        <a href="{{ url('/user/student/krs/detail/print') }}" class="btn btn-primary btn-sm"> <i class="fas fa-print"></i> Cetak KRS</a>
                        <a href="{{ url('/user/student/krs') }}" class="btn btn-danger btn-sm ml-3"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
                    </div>                                      
                </div>
            </div>  
        @endcard
        </form>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.table').DataTable({
        "language": {
            "zeroRecords": "<strong>Data tidak ditemukan</strong>"
        }
    })

    $('.wrapper').addClass('sidebar_minimize')
    $('label.offset-sm-1').append(` <span class="required-label">*</span>`)
    $('.form-group .form-control').addClass('form-control-sm')
</script>
@endpush