@extends('layouts._students.master')

@section('content')
<div class="page-inner mt-2">
    <div class="col-12">
        @card
            @slot('title')
            Edit Profile
            @endslot
            
            <form method="post" data-toggle="validator" action="{{ route('students.update', $student->id) }}" enctype="multipart/form-data">
                @csrf @method('put')
                <div class="form-group row">
                    <label for="nim" class="col-sm-2 col-form-label">NIM</label>
                    <div class="col-sm-6">
                        <input class="form-control" name="nim" id="nim" value="{{ $student->nim }}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="nama" name="nama" value="{{ $student->nama }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="tempat_lahir" class="col-sm-2 col-form-label">Tempat Lahir</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value="{{ $student->tempat_lahir }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="datepicker" class="col-sm-2 col-form-label">Tanggal Lahir </label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <input type="text" class="form-control" id="datepicker" name="tanggal_lahir" value="{{ $student->tanggal_lahir }}">
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-8">
                        <textarea name="alamat" id="alamat" class="form-control" rows="5">{{ $student->alamat }}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="telpon" class="col-sm-2 col-form-label">No Telpon</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="telpon" name="telpon" value="{{ $student->telpon }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="angkatan" class="col-sm-2 col-form-label">Tahun Angkatan</label>
                    <div class="col-sm-4">
                        <select name="angkatan" id="angkatan" class="form-control">
                            @for ($i = date('Y'); $i >= date('Y') - 10; $i--)
                                <option value="{{ $i }}" {{ $i == $student->angkatan ? 'selected' : ''  }}>{{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="semester" class="col-sm-2 col-form-label">Semester</label>
                    <div class="col-sm-4">
                        <select name="semester" id="semester" class="form-control">
                            @for ($i = 1; $i <= 8; $i++)    
                                <option value="{{ $i }}" {{ $i == $student->semester ? 'selected' : '' }}>{{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="jurusan" class="col-sm-2 col-form-label">Jurusan</label>
                    <div class="col-sm-6">
                        <select class="form-control" id="jurusan" name="kode_jurusan" required>
                            @foreach ($departments as $department)
                                <option value="{{ $department->kode }}" {{ $department->kode == $student->kode_jurusan ? 'selected' : '' }}>{{ $department->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="kelas" class="col-sm-2 col-form-label">Kelas</label>
                    <div class="col-sm-6">
                        <select class="form-control" id="kelas" name="kelas_id">
                            @foreach ($class_times as $class_time)
                                <option value="{{ $class_time->id }}" {{ $class_time->id == $student->kelas_id ? 'selected' : '' }}>{{ $class_time->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="jk" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="laki-laki" name="jenis_kelamin" class="custom-control-input" value="L" {{ $student->jenis_kelamin == 'L' ? 'checked' : '' }}>
                        <label class="custom-control-label" for="laki-laki">Laki-laki</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="Perempuan" name="jenis_kelamin" class="custom-control-input" value="P" {{ $student->jenis_kelamin == 'P' ? 'checked' : '' }}>
                        <label class="custom-control-label" for="Perempuan">Perempuan</label>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="email" name="email" value="{{ $student->email }}" readonly>
                    </div>
                </div>
                <div class="form-group  row">
                    <label class="col-sm-2">Upload Image</label>
                    <div class="col-sm-4">
                        <div class="input-file input-file-image mt--3">
                            <input type="file" class="form-control form-control-file" id="foto" name="foto">
                            <label for="foto" class="img-circle" style="cursor: pointer;"><i class="fa fa-file-image"></i>
                                <img class="img-upload-preview rounded" height="200" src="{{ asset('assets/img/uploads/') }}/{{ $student->foto }}" alt="preview">
                            </label>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-sm-6 offset-sm-3">
                            <button class="btn btn-primary"> <i class="far fa-save"></i> Simpan</button>
                            @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
                            <a href="{{ url('/students') }}" class="btn btn-danger ml-3"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
                            
                            @elseif(Auth::user()->role_id == 3)
                            <a href="{{ url('/user/student/profile') }}" class="btn btn-danger ml-3"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
                            
                            @else
                            <a href="{{ url('/user/lecturer/profile') }}" class="btn btn-danger ml-3"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
                            @endif
                        </div>                                      
                    </div>
                </div>
            </form>
        @endcard
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.profile').parent().addClass('active');
    
    $('label.col-sm-2').addClass('offset-sm-1')
    $('label.offset-sm-1').append(` <span class="required-label">*</span>`)
    $('.form-group .form-control').addClass('form-control-sm')
</script>
@endpush