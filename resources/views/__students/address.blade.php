@extends('layouts._students.master')

@push('css')
    <style>
        .form-check, .form-group {
            padding: 0;
        }
        .form-check label, .form-group label {
            font-weight: normal;
        }
    </style>
@endpush

@section('content')
<div class="row mt-2 d-lg-block d-none"></div>
<div class="page-inner">
    <div class="row">
        <div class="col-12">
            @card
                @slot('title')
                    Alamat {{ ucfirst(Auth::user()->name) }}
                @endslot
                
                <form action="#" method="post">
                    @csrf 
                    <div class="form-group row">
                        <label for="ktp" class="col-sm-4 col-form-label">No NIK / KTP <small>(Tanpa Tanda Baca)</small></label>
                        <div class="col-sm-6">
                            <input class="form-control" name="ktp" id="ktp">
                        </div>
                    </div>
                    <div class="form-group row mb-3">
                        <label for="alamat" class="col-sm-4 col-form-label">Alamat / Jalan / Gang</label>
                        <div class="col-sm-6">
                            <textarea class="form-control" name="alamat" id="alamat" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="dusun" class="col-sm-4 col-form-label">Dusun / Kampung</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="dusun" id="dusun">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="rt" class="col-sm-4 col-form-label">RT</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="rt" id="rt">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="rw" class="col-sm-4 col-form-label">RW</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="rw" id="rw">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="kode_pos" class="col-sm-4 col-form-label">Kode Pos</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="kode_pos" id="kode_pos">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="desa" class="col-sm-4 col-form-label">Kelurahan / Desa</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="desa" id="desa">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="kecamatan" class="col-sm-4 col-form-label">Kecamatan</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="kecamatan" id="kecamatan">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="kabupaten" class="col-sm-4 col-form-label">Kabupaten</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="kabupaten" id="kabupaten">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="provinsi" class="col-sm-4 col-form-label">Provinsi</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="provinsi" id="provinsi">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="telpon" class="col-sm-4 col-form-label">Telepon Rumah, jika tidak ada, nomor yang bisa dihubungi.</label>
                        <div class="col-sm-6">
                            <input class="form-control" name="telpon" id="telpon">
                        </div>
                    </div>
                </form>
            @endcard
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.col-form-label').addClass('offset-md-1')
    $('label.offset-md-1').append(` <span class="required-label">*</span>`)
    $('.form-group .form-control').addClass('form-control-sm')
</script>
@endpush