@extends('layouts._students.master')

@section('content')
<div class="page-inner mt-2">
   <div class="row">
        <div class="col-12">
            @card
                @slot('title')
                    Detail Mahasiswa
                @endslot

                
                <div class="row">
                    <div class="col-md-4">
                        <img src="{{ asset('/assets/img/uploads/') }}/{{ $student->foto }}" class="card-img" alt="">
                    </div>
                    <div class="col-md-8">
                        
                        <table width="50%" class="table table-bordered">
                            <tr>
                                <td>Nama</td>
                                <td>: {{ ucfirst($student->nama) }}</td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>: {{ $student->alamat }}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>: {{ $student->email }}</td>
                            </tr>
                            <tr>
                                <td>Tanggal Lahir</td>
                                <td>: {{ date('F d, Y', strtotime($student->tanggal_lahir)) }}</td>
                            </tr>
                            <tr>
                                <td>Jurusan</td>
                                <td>: 
                                    @foreach ($departments as $department)
                                        {{ $department->kode == $student->kode_jurusan ? $department->nama : '' }}
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>NIM</td>
                                <td>: {{ $student->nim }}</td>
                            </tr>
                            <tr>
                                <td>Member Pada</td>
                                <td>: {{ $student->created_at }}</td>
                            </tr>
                            <tr>
                                <td>Semester</td>
                                <td>: {{ $student->semester }}</td>
                            </tr>
                            <tr>
                                <td>Jenis Kelamin</td>
                                <td>: {{ $student->jenis_kelamin == 'L' ? 'Laki-laki' : 'Perempuan' }}</td>
                            </tr>
                            <tr>
                                <td>Angkatan</td>
                                <td>: {{ $student->angkatan }}</td>
                            </tr>
                        </table>
                        
                    </div>
                </div>
                
                
                <div class="row">
                    <a href="{{ url('/students') }}" class="btn btn-danger ml-3"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
                    
                    @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
                    <a onclick="print('{{ url('/students/print/ktm/') }}/{{ $student->id }}')" class="btn btn-white border ml-2"><i class="fas fa-print"></i> Cetak KTM</a>
                    
                    @elseif(Auth::user()->role_id == 3)
                    <a href="{{ route('students.edit_profile', $student->id) }}" class="btn btn-white border ml-5"><i class="fas fa-user-edit"></i> Edit Profile</a>
                    <a href="{{ url('/user/student/password/edit') }}" class="btn btn-white border ml-2"><i class="fas fa-key"></i> Edit Password</a>
                    @endif
                </div>
            @endcard
        </div>
   </div> 
</div>
@endsection

@push('scripts')
    <script>
        $('.profile').parent().addClass('active');

        function print(url) {
            window.open(url, "Nota PDF", "height=675, width=1024, left=175, scrollbars=yes");
        }
    </script>
@endpush