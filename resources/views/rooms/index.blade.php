@extends('layouts.master')

@section('content')
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Data Ruangan</h2>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <a href="{{ route('rooms.create') }}" class="btn btn-white btn-sm btn-border"><i class="fas fa-plus-circle"></i> Tambah</a>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-12">
            @card
                @slot('title')
                    
                @endslot

                @table
                    @slot('thead')
                        <th>No</th>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Aksi</th>
                    @endslot
                    
                    @php $no = 1; @endphp
                    @foreach ($rooms as $room)
                        <tr>
                            <td width="5%">{{ $no++ }}.</td>
                            <td width="25%">{{ $room->kode }}</td>
                            <td>{{ $room->nama }}</td>
                            <td width="5%">
                                <form method="POST" action="{{ route('rooms.destroy', $room->id) }}">
                                    @csrf @method('delete')
                                    <div class="form-button-action text-center">
                                        <a href="{{ route('rooms.edit', $room->id) }}" class="btn btn-link btn-primary" title="Edit"><i class="far fa-edit"></i></a>
                                        <button class="btn btn-link btn-danger" title="Delete" onclick="return confirm('Yakin?')"><i class="fas fa-trash-alt"></i></button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endtable
            @endcard
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.table').DataTable({
        "language": {
            "zeroRecords": "<strong>Data tidak ditemukan</strong>"
        }
    })

    $('.card-header').addClass('d-none')
</script>
@endpush