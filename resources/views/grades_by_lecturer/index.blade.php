@extends('layouts.master')

@section('content')
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Daftar Mata Kuliah Diajar</h2>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <button class="btn btn-sm btn-border border-white text-white" disabled style="cursor: not-allowed;"><i class="fas fa-plus-circle"></i> Tambah</button>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-12">
            @card
                @slot('title')
                    
                @endslot

                @table
                    @slot('thead')
                        <th>No</th>
                        <th>Kode Matkul</th>
                        <th>Mata Kuliah</th>
                        <th>SKS</th>
                        <th>Semester</th>
                        <th>Kelas</th>
                        <th>Kode Dosen</th>
                        <th>Dosen</th>
                        <th>Ruang</th>
                        <th>Jadwal</th>
                        <th>Aksi</th>
                    @endslot
                    
                    @php $no = 1; @endphp
                    @foreach ($courses as $course)
                        @if (Auth::user()->user_id == $course->nidn)
                            <tr>
                                <td width="5%">{{ $no++ }}.</td>
                                <td>{{ $course->kode_mk }}</td>
                                <td>{{ $course->nama_mk }}</td>
                                <td>{{ $course->sks }}</td>
                                <td>{{ $course->semester }}</td>
                                <td>{{ $course->nama_kelas }}</td>
                                <td>{{ $course->kode_dosen }}</td>
                                <td>{{ $course->nama_dosen }}</td>
                                <td>{{ $course->nama_ruang }}</td>
                                <td style="font-size: .8em;"><span class="badge badge-dark border rounded">{{ date('d F Y, ', strtotime($course->tanggal)) . $course->jam_mulai .' - '. $course->jam_selesai }} WIB</span></td>
                                <td width="5%">
                                    <div class="form-button-action text-center">
                                        <a href="{{ route('grades.edit', $course->id_dosen) }}?kode_mk={{ $course->kode_mk }}" class="btn btn-primary btn-link btn-lg" title="Nilai Mahasiswa"><i class="fas fa-arrow-circle-right"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                @endtable
            @endcard
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.table').DataTable({
        "language": {
            "zeroRecords": "<strong>Data tidak ditemukan</strong>"
        }
    })

    $('.wrapper').addClass('sidebar_minimize')
    $('.card-header').addClass('d-none')
</script>
@endpush