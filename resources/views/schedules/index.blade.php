@extends('layouts.master')

@section('content')
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Jadwal Perkuliahan</h2>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <a href="{{ route('schedules.create') }}" class="btn btn-white btn-sm btn-border"><i class="fas fa-plus-circle"></i> Tambah</a>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-12">
            @card
                @slot('title')
                    
                @endslot

                @table
                    @slot('thead')
                        <th>No</th>
                        <th>Mata Kuliah</th>
                        <th>Kelas</th>
                        <th>Jurusan</th>
                        <th>Semester</th>                    
                        <th>Ruang</th>
                        <th>Hari</th>                   
                        <th>Jam</th>
                        <th>Dosen</th>
                        <th>TA Akademik</th>
                        <th>Aksi</th>
                    @endslot
                    
                    @php $no = 1; @endphp
                    @foreach ($schedules as $schedule)
                    @if(Auth::user()->user_id == '$2y$10$v38CO6FaWHNWk1WUht2TaeDHENpfQ2o1042QhdFKFzdxjkYqvyLge' || 
                        Auth::user()->user_id == '$2y$10$rmwWOn3Wki/xrIWoY3PU1OgmoXyraQbppisUAwRdFiINCMNbV43F.') 
                        <tr>
                            <td width="5%">{{ $no++ }}.</td>
                            <td>
                                @foreach ($courses as $course)
                                    {{ $course->kode == $schedule->kode_mk ? $course->nama : ''}}
                                @endforeach
                            </td>
                            <td>
                                @foreach ($class_times as $time)
                                    {{ $time->id == $schedule->kelas_id ? $time->nama : ''}}
                                @endforeach
                            </td>
                            <td>
                                @foreach ($departments as $department)
                                    {{ $department->kode == $schedule->kode_jurusan ? $department->nama : ''}}
                                @endforeach
                            </td>
                            <td>{{ $schedule->semester }}</td>
                            <td>
                                @foreach ($rooms as $room)
                                    {{ $room->kode == $schedule->kode_ruang ? $room->nama : ''}}
                                @endforeach
                            </td>
                            <td>{{ $schedule->hari }}</td>
                            <td>{{ $schedule->jam_mulai .' - '. $schedule->jam_selesai }}</td>
                            <td>
                                @foreach ($lecturers as $lecturer)
                                    {{ $lecturer->kode == $schedule->kode_dosen ? $lecturer->nama : ''}}
                                @endforeach
                            </td>
                            <td>{{ $schedule->tahun_akademik }}</td>
                            <td width="5%">
                                <form method="POST" action="{{ route('schedules.destroy', $schedule->id) }}">
                                    @csrf @method('delete')
                                    <div class="form-button-action text-center">
                                        <a href="{{ route('schedules.edit', $schedule->id) }}" class="btn btn-link btn-primary" title="Edit"><i class="far fa-edit"></i></a>
                                        <button class="btn btn-link btn-danger" title="Delete" onclick="return confirm('Yakin?')"><i class="fas fa-trash-alt"></i></button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        @else
                            @foreach ($lecturers as $lecturer)
                                @if (Auth::user()->user_id == $lecturer->nidn && $lecturer->kode == $schedule->kode_dosen)
                                    <tr>
                                        <td width="5%">{{ $no++ }}.</td>
                                        <td>
                                            @foreach ($courses as $course)
                                                {{ $course->kode == $schedule->kode_mk ? $course->nama : ''}}
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach ($class_times as $time)
                                                {{ $time->id == $schedule->kelas_id ? $time->nama : ''}}
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach ($departments as $department)
                                                {{ $department->kode == $schedule->kode_jurusan ? $department->nama : ''}}
                                            @endforeach
                                        </td>
                                        <td>{{ $schedule->semester }}</td>
                                        <td>
                                            @foreach ($rooms as $room)
                                                {{ $room->kode == $schedule->kode_ruang ? $room->nama : ''}}
                                            @endforeach
                                        </td>
                                        <td>{{ $schedule->hari }}</td>
                                        <td>{{ $schedule->jam_mulai .' - '. $schedule->jam_selesai }}</td>
                                        <td>
                                            @foreach ($lecturers as $lecturer)
                                                {{ $lecturer->kode == $schedule->kode_dosen ? $lecturer->nama : ''}}
                                            @endforeach
                                        </td>
                                        <td>{{ $schedule->tahun_akademik }}</td>
                                        <td width="5%">
                                            <form method="POST" action="{{ route('schedules.destroy', $schedule->id) }}">
                                                @csrf @method('delete')
                                                <div class="form-button-action text-center">
                                                    <a href="{{ route('schedules.edit', $schedule->id) }}" class="btn btn-link btn-primary" title="Edit"><i class="far fa-edit"></i></a>
                                                    <button class="btn btn-link btn-danger" title="Delete" onclick="return confirm('Yakin?')"><i class="fas fa-trash-alt"></i></button>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                @endtable
            @endcard
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.table').DataTable({
        "language": {
            "zeroRecords": "<strong>Data tidak ditemukan</strong>"
        }
    })

    $('.wrapper').addClass('sidebar_minimize')
    $('.card-header').addClass('d-none')
</script>
@endpush