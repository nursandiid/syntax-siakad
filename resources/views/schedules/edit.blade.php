@extends('layouts.master')

@section('content')
<div class="page-inner mt-2">
    <div class="col-12">
        @card
            @slot('title')
                Edit Jadwal Perkuliahan
            @endslot
            
            <form method="post" action="{{ route('schedules.update', $schedule->id) }}">
                @csrf @method('put')
                <div class="form-group row">
                    <label for="matkul" class="col-sm-2 col-form-label">Mata Kuliah</label>
                    <div class="col-sm-4">
                        <select class="form-control" id="matkul" name="kode_mk">
                            @foreach ($courses as $course)
                                <option value="{{ $course->kode }}" {{ $course->kode == $schedule->kode_mk ? 'selected' : '' }}>{{ $course->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="kelas" class="col-sm-2 col-form-label">Kelas</label>
                    <div class="col-sm-4">
                        <select class="form-control" id="kelas" name="kelas_id">
                            @foreach ($class_times as $time)
                                <option value="{{ $time->id }}" {{ $time->id == $schedule->kelas_id ? 'selected' : '' }}>{{ $time->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="ruang" class="col-sm-2 col-form-label">Ruang</label>
                    <div class="col-sm-6">
                        <select class="form-control" id="ruang" name="kode_ruang">
                            @foreach ($rooms as $room)
                                <option value="{{ $room->kode }}" {{ $room->kode == $schedule->kode_ruang ? 'selected' : '' }}>{{ $room->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="kode_dosen" class="col-sm-2 col-form-label">Dosen</label>
                    <div class="col-sm-6">
                        <select class="form-control" id="kode_dosen" name="kode_dosen">
                            @foreach ($lecturers as $lecturer)
                                <option value="{{ $lecturer->kode }}" {{ $lecturer->kode == $schedule->kode_dosen ? 'selected' : '' }}>{{ $lecturer->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="semester" class="col-sm-2 col-form-label">Semester</label>
                    <div class="col-sm-4">
                        <select name="semester" id="semester" class="form-control" required>
                            @for ($i = 1; $i <= 8; $i++)    
                                <option value="{{ $i }}" {{ $i == $schedule->semester ? 'selected' : '' }}>{{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="jurusan" class="col-sm-2 col-form-label">Jurusan</label>
                    <div class="col-sm-6">
                        <select class="form-control" id="jurusan" name="kode_jurusan" required>
                            @foreach ($departments as $department)
                                <option value="{{ $department->kode }}" {{ $department->kode == $schedule->kode_jurusan ? 'selected' : '' }}>{{ $department->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="hari" class="col-sm-2 col-form-label">Hari</label>
                    <div class="col-sm-4">
                        <select class="form-control" id="hari" name="hari">
                            @foreach ($days as $day)
                                <option value="{{ $day }}" {{ $day == $schedule->hari ? 'selected' : '' }}>{{ $day }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="input-group">
                        <label for="jam_mulai" class="col-sm-2 col-form-label">Jam Mulai</label>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <input type="text" class="form-control timepicker" id="jam_mulai" name="jam_mulai" value="{{ $schedule->jam_mulai }}">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fa fa-clock"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="input-group">
                        <label for="jam_selesai" class="col-sm-2 col-form-label">Jam Selesai</label>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <input type="text" class="form-control timepicker" id="jam_selesai" name="jam_selesai" value="{{ $schedule->jam_selesai }}">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fa fa-clock"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="tahun_akademik" class="col-sm-2 col-form-label">Tahun Akademik</label>
                    <div class="col-sm-4">
                        <select name="tahun_akademik" id="tahun_akademik" class="form-control">
                            @foreach ($years as $year)
                                <option value="{{ $year->tahun }}" {{ $year->tahun == $schedule->tahun_akademik ? 'selected' : '' }}>{{ $year->tahun }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-sm-6 offset-sm-3">
                            <button class="btn btn-primary"> <i class="far fa-save"></i> Simpan</button>
                            <a href="{{ url('/schedules') }}" class="btn btn-danger ml-3"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
                        </div>                                      
                    </div>
                </div>
            </form>
        @endcard
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('label.col-sm-2').addClass('offset-sm-1')
    $('label.offset-sm-1').append(` <span class="required-label">*</span>`)
    $('.form-group .form-control').addClass('form-control-sm')
</script>
@endpush