@extends('layouts.master')

@section('content')
<div class="page-inner mt-2">
    <div class="col-12">
        @card
            @slot('title')
                Set Nomor Mahasiswa
            @endslot
            
            <form method="post" action="{{ route('students.update_certificate', $student->id) }}">
                @csrf @method('put')
                <div class="form-group row">
                    <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-6">
                        <input class="form-control" id="nama" value="{{ $student->nama }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="no_ijazah" class="col-sm-2 col-form-label">No Ijazah</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="no_ijazah" name="no_ijazah" value="{{ $student->no_ijazah }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="judul_skripsi" class="col-sm-2 col-form-label">Judul Skripsi</label>
                    <div class="col-sm-6">
                        <textarea name="judul_skripsi" id="judul_skripsi" rows="3" class="form-control">{{ $student->judul_skripsi }}</textarea>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-sm-6 offset-sm-3">
                            <button class="btn btn-primary"> <i class="far fa-save"></i> Simpan</button>
                            <a href="{{ url('/students/certificates') }}" class="btn btn-danger ml-3"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
                        </div>                                      
                    </div>
                </div>
            </form>
        @endcard
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('label.col-sm-2').addClass('offset-sm-1')
    $('label.offset-sm-1').append(` <span class="required-label">*</span>`)
    $('.form-group .form-control').addClass('form-control-sm')
</script>
@endpush