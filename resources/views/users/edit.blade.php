@extends('layouts.master')

@section('content')
<div class="page-inner mt-2">
    <div class="col-12">
        @card
            @slot('title')
            @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
                Edit User
            @else
                Edit Password
            @endif
            @endslot
            
            <form method="post" action="{{ route('users.update', $user->id) }}" data-toggle="validator">
                @csrf @method('put')
                <div class="form-group row">
                    <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-6">
                        <input class="form-control" id="nama" value="{{ $user->name }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-6">
                        <input type="password" name="password" id="password" class="form-control" required minlength="6">
                        <span class="help-block with-errors"></span>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password1" class="col-sm-2 control-label">Ulangi Password</label>
                    <div class="col-sm-6">
                        <input type="password" name="password1" id="password1" class="form-control" data-match="#password" data-match="#password" required>
                        <span class="help-block with-errors"></span>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-sm-6 offset-sm-3">
                            <button class="btn btn-primary"> <i class="far fa-save"></i> Simpan</button>
                            
                            @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
                            <a href="{{ url('/users') }}" class="btn btn-danger ml-3"><i class="fas fa-arrow-circle-left"></i> Kembali</a>    
                            @else
                            <a href="{{ url('/user/student/profile') }}" class="btn btn-danger ml-3"><i class="fas fa-arrow-circle-left"></i> Kembali</a>
                            @endif
                        </div>                                      
                    </div>
                </div>
            </form>
        @endcard
    </div>
</div>
@endsection

@push('scripts')
<script>
    $('.profile').parent().addClass('active');
    $('label.col-sm-2').addClass('offset-sm-1')
    $('label.offset-sm-1').append(` <span class="required-label">*</span>`)
    $('.form-group .form-control').addClass('form-control-sm')
</script>
@endpush