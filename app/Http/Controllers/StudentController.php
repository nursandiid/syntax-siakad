<?php

namespace App\Http\Controllers;

use App\Student;
use App\Department;
use App\Class_time;
use App\User;
use App\KRS;
use Illuminate\Http\Request;
use File;
use PDF;
use DB;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::latest()->get();
        $departments = Department::all();
        return view('students.index', compact('students', 'departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        $class_times = Class_time::all();
        return view('students.create', compact('departments', 'class_times'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $foto = null;
        if ($request->hasFile('foto')) {
            $foto = $this->saveFile($request->name, $request->file('foto'));
        }

        $students = Student::create([
            'nim' => $request->nim,
            'nama' => $request->nama,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'alamat' => $request->alamat,
            'kode_jurusan' => $request->kode_jurusan,
            'semester' => $request->semester,
            'jenis_kelamin' => $request->jenis_kelamin,
            'email' => $request->email,
            'telpon' => $request->telpon,
            'angkatan' => $request->angkatan,
            'foto' => $foto,
            'krs' => 1,
            'khs' => 1,
            'kelas_id' => $request->kelas_id
        ]);

        $user = User::create([
            'name' => $request->nama,
            'email' => $request->email,
            'user_id' => $request->nim,
            'password' => bcrypt($request['password']),
            'image' => $foto,
            'role_id' => 3
        ]);

        return redirect(url('/students'));
    }

    private function saveFile($name, $photo)
    {
        $images = str_slug($name) . time() .'.' . $photo->getClientOriginalExtension();
        $photo->move(public_path('assets/img/uploads'), $images);
        return $images;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        $departments = Department::all();
        return view('students.detail', compact('student', 'departments'));
    }

    public function detail()
    {
        $student = Student::where('nim', \Auth::user()->user_id)->first();
        $departments = Department::all();

        if (\Auth::user()->role_id == 3) {
            return view('__students.detail', compact('student', 'departments'));
        } else return view('students.detail', compact('student', 'departments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        $departments = Department::all();
        $class_times = Class_time::all();
        return view('students.edit', compact('student', 'departments', 'class_times'));
    }

    public function edit_profile()
    {
        $student = Student::where('nim', \Auth::user()->user_id)->first();
        $departments = Department::all();
        $class_times = Class_time::all();
        return view('__students.edit', compact('student', 'departments', 'class_times'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $foto = $student->image;
        if ($request->hasFile('foto')) {
            !empty($foto) ? File::delete(public_path('assets/img/uploads/' . $foto)) : null; 
            $foto = $this->saveFile($request->nama, $request->file('foto'));
        }

        $student->update([
            'nim' => $request->nim,
            'nama' => $request->nama,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'alamat' => $request->alamat,
            'kode_jurusan' => $request->kode_jurusan,
            'semester' => $request->semester,
            'jenis_kelamin' => $request->jenis_kelamin,
            'email' => $request->email,
            'telpon' => $request->telpon,
            'angkatan' => $request->angkatan,
            'foto' => $foto,
            'krs' => 1,
            'khs' => 1,
            'kelas_id' => $request->kelas_id
        ]);

        $user = \Auth::user()->where('user_id', $student->nim)->update([
            'name' => $request->nama,
            'image' => $foto,
        ]);

        if(\Auth::user()->role_id == 3) return redirect(url('/user/student/profile'));
        else return redirect(url('/students'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->delete();
        return redirect()->back();
    }

    public function certificate()
    {
        $students = Student::latest()->get();
        return view('certificates.index', compact('students'));
    }

    public function set_certificate($id)
    {
        $student = Student::find($id);
        return view('certificates.edit', compact('student'));
    }

    public function update_certificate(Request $request, $id)
    {
        $student = Student::find($id);
        $student->update([
            'no_ijazah' => $request->no_ijazah,
            'judul_skripsi' => $request->judul_skripsi
        ]);

        return redirect(url('/students/certificates'));
    }

    public function show_KTM()
    {
        $students = Student::all();
        $departments = Department::all();

        return view('students.show_ktm', compact('students', 'departments'));
    }

    public function printKTM($id)
    {
        $student = Student::find($id);
        $departments = Department::all();

        // $pdf = PDF::loadView('students.print_ktm', compact('student', 'departments'));
        // $pdf->setPaper(array(0,0,600,440), 'potrait');
        // return $pdf->stream();

        return view('students.print_ktm', compact('student', 'departments'));
    }

























    // public function Cek()
    // {
    //     $detail = Cek::with('produk')
    //             ->where('id_penjualan', '=', session('idpenjualan'))
    //             ->get();
                
    //     $penjualan = Cek::find(session('idpenjualan'));
    //     $setting = Setting::first();
    //     $no = 0;

    //     $pdf = PDF::loadView('penjualan_detail.notapdf', compact('detail', 'penjualan', 'setting', 'no'));
    //     $pdf->setPaper(array(0,0,609,440), 'potrait');
    //     return $pdf->stream();
    // }
}
