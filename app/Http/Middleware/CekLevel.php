<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CekLevel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role1, $role2 = 2)
    {
        if (Auth::user()) {
            if(Auth::user()->role_id == $role1 || Auth::user()->role_id == $role2)
            return $next($request);
        }
        
        return redirect('/');
    }
}
