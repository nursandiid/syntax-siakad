-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 03, 2019 at 09:28 AM
-- Server version: 10.3.13-MariaDB-2
-- PHP Version: 7.2.19-1+ubuntu19.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siakad`
--

-- --------------------------------------------------------

--
-- Table structure for table `class_times`
--

CREATE TABLE `class_times` (
  `id` int(1) NOT NULL,
  `nama` varchar(15) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `class_times`
--

INSERT INTO `class_times` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'Pagi', '2019-06-13 17:00:00', '2019-06-13 17:00:00'),
(2, 'Siang', '2019-06-13 17:00:00', '2019-06-13 17:00:00'),
(3, 'Sore', '2019-06-13 17:00:00', '2019-06-13 17:00:00'),
(4, 'Malam', '2019-06-13 17:00:00', '2019-06-13 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `nama` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `sks` varchar(1) COLLATE latin1_general_ci NOT NULL,
  `semester` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `kode_jurusan` varchar(4) COLLATE latin1_general_ci NOT NULL,
  `kode_kuri` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `kategori` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `kode`, `nama`, `sks`, `semester`, `kode_jurusan`, `kode_kuri`, `kategori`, `created_at`, `updated_at`) VALUES
(1, 'M001', 'Matematika', '3', '1', 'S1M', 'K2013', NULL, '2019-06-14 23:52:59', '2019-06-14 23:52:59'),
(5, 'F001', 'Fisika Dasar', '2', '1', 'S1A', 'K2017', NULL, '2019-06-14 23:52:59', '2019-06-14 23:52:59'),
(6, 'A0LGD', 'Algoritma Dasar', '2', '1', 'SIM', 'K2017', NULL, '2019-06-24 20:12:22', '2019-06-24 20:12:22'),
(7, 'P00as', 'Pemrograman Pascal', '2', '1', 'SIM', NULL, NULL, '2019-06-25 03:00:57', '2019-06-25 03:00:57'),
(8, 'PW1b', 'Pemrograman Web', '2', '1', 'SIM', NULL, NULL, '2019-06-25 07:28:24', '2019-06-25 07:28:24');

-- --------------------------------------------------------

--
-- Table structure for table `curricula`
--

CREATE TABLE `curricula` (
  `id` int(1) NOT NULL,
  `kode` varchar(5) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `curricula`
--

INSERT INTO `curricula` (`id`, `kode`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'K2013', 'Kurikulum 2013', '2019-06-13 21:27:05', '2019-06-13 21:27:05'),
(2, 'K2017', 'Kurikulum 2017', '2019-06-13 21:27:05', '2019-06-13 21:27:05'),
(10, 'K2018', 'Kurikulum 2018', '2019-06-13 21:27:05', '2019-06-13 21:27:05');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `nama` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `sebutan_lulusan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `kode`, `nama`, `sebutan_lulusan`, `created_at`, `updated_at`) VALUES
(24, 'SIM', 'Sistem Informasi', NULL, '2019-06-24 20:11:50', '2019-06-24 20:11:50'),
(18, 'D3M', 'D3 Manajemen Perkantoran', '', NULL, NULL),
(19, 'S1M', 'S1 Manajemen', 'Sarjana Ekonomi (SE)', NULL, NULL),
(20, 'S1A', 'S1 Akuntansi', '', NULL, NULL),
(23, 'TI', 'Teknik Industri', NULL, '2019-06-24 20:11:32', '2019-06-24 20:11:32');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` int(11) NOT NULL,
  `nim` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `kode_mk` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `kode_dosen` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `semester` int(2) NOT NULL,
  `presensi` int(11) NOT NULL DEFAULT 0,
  `tugas` int(11) NOT NULL,
  `quiz` int(11) NOT NULL DEFAULT 0,
  `uts` int(11) NOT NULL,
  `uas` int(11) NOT NULL,
  `nilaiakhir` float NOT NULL DEFAULT 0,
  `grade` varchar(2) COLLATE latin1_general_ci DEFAULT NULL,
  `ket` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `kelas` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `prepre` float NOT NULL DEFAULT 0,
  `pretug` float NOT NULL DEFAULT 0,
  `prequi` float NOT NULL DEFAULT 0,
  `preuts` float NOT NULL DEFAULT 0,
  `preuas` float NOT NULL DEFAULT 0,
  `tahun_akad` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `nim`, `kode_mk`, `kode_dosen`, `semester`, `presensi`, `tugas`, `quiz`, `uts`, `uas`, `nilaiakhir`, `grade`, `ket`, `kelas`, `prepre`, `pretug`, `prequi`, `preuts`, `preuas`, `tahun_akad`, `created_at`, `updated_at`) VALUES
(11, '234111678', 'M001', 'd002', 1, 90, 85, 85, 80, 85, 85.75, NULL, 'Baru', 'Pagi', 0.15, 0.1, 0.05, 0.3, 0.4, NULL, '2019-06-23 12:06:33', '2019-06-23 12:06:33'),
(9, '234111789', 'M001', 'd002', 1, 90, 80, 80, 85, 80, 84.5, NULL, 'Baru', 'Pagi', 0.15, 0.1, 0.05, 0.3, 0.4, NULL, '2019-06-23 11:54:31', '2019-06-23 11:54:31'),
(12, '234111678', 'F001', 'd001', 1, 80, 85, 80, 80, 80, 83.5, NULL, 'Baru', 'Pagi', 0.15, 0.1, 0.05, 0.3, 0.4, NULL, '2019-06-23 12:53:38', '2019-06-23 12:53:38'),
(13, '234111789', 'F001', 'd001', 1, 90, 80, 80, 80, 80, 83, NULL, 'Baru', 'Pagi', 0.15, 0.1, 0.05, 0.3, 0.4, NULL, '2019-06-23 12:54:14', '2019-06-23 12:54:14'),
(16, '999123456', 'PW1b', 'd002', 1, 80, 80, 80, 75, 90, 85.5, NULL, 'Baru', NULL, 0.15, 0.1, 0.05, 0.3, 0.4, NULL, '2019-06-25 08:47:37', '2019-06-25 08:47:37'),
(17, '999123456', 'A0LGD', 'd002', 1, 80, 80, 75, 89, 88, 88.65, NULL, 'Baru', NULL, 0.15, 0.1, 0.05, 0.3, 0.4, NULL, '2019-06-25 08:48:09', '2019-06-27 23:27:56'),
(18, '999123456', 'P00as', 'd002', 1, 76, 89, 80, 80, 75, 81.9, NULL, 'Baru', NULL, 0.15, 0.1, 0.05, 0.3, 0.4, NULL, '2019-06-25 08:48:27', '2019-06-25 08:48:27');

-- --------------------------------------------------------

--
-- Table structure for table `khs`
--

CREATE TABLE `khs` (
  `id` int(11) NOT NULL,
  `nim` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `kode_mk` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `nilai` varchar(1) COLLATE latin1_general_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `krs`
--

CREATE TABLE `krs` (
  `id` int(9) NOT NULL,
  `kode_mk` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `nim` varchar(12) COLLATE latin1_general_ci NOT NULL,
  `kode_jurusan` varchar(4) COLLATE latin1_general_ci NOT NULL,
  `status_nilai` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `status_krs` int(2) DEFAULT 0,
  `ket` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `kelas_id` varchar(2) COLLATE latin1_general_ci NOT NULL,
  `presensi` float NOT NULL DEFAULT 0,
  `tugas` float NOT NULL DEFAULT 0,
  `quiz` float NOT NULL DEFAULT 0,
  `uts` float NOT NULL DEFAULT 0,
  `uas` float NOT NULL DEFAULT 0,
  `tahun_akad` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `krs`
--

INSERT INTO `krs` (`id`, `kode_mk`, `nim`, `kode_jurusan`, `status_nilai`, `status_krs`, `ket`, `kelas_id`, `presensi`, `tugas`, `quiz`, `uts`, `uas`, `tahun_akad`, `created_at`, `updated_at`) VALUES
(4, 'M001', '234111789', 'S1A', '0', 1, 'Baru', '1', 0.15, 0.1, 0.05, 0.3, 0.4, '2018/2019', '2019-06-22 00:31:52', '2019-06-25 08:44:43'),
(2, 'M001', '234111678', 'S1A', '0', 1, 'Baru', '1', 0.15, 0.1, 0.05, 0.3, 0.4, '2018/2019', '2019-06-21 05:59:06', '2019-06-25 08:44:43'),
(3, 'F001', '234111678', 'S1A', '0', 1, 'Baru', '1', 0.15, 0.1, 0.05, 0.3, 0.4, '2018/2019', '2019-06-21 05:59:06', '2019-06-25 08:44:43'),
(5, 'F001', '234111789', 'S1A', '0', 1, 'Baru', '1', 0.15, 0.1, 0.05, 0.3, 0.4, '2018/2019', '2019-06-22 00:31:52', '2019-06-25 08:44:43'),
(11, 'A0LGD', '999123456', 'SIM', '0', 1, 'Baru', '2', 0.15, 0.1, 0.05, 0.3, 0.4, '2018/2019', '2019-06-25 02:32:02', '2019-06-25 08:48:09'),
(12, 'P00as', '999123456', 'SIM', '0', 1, 'Baru', '2', 0.15, 0.1, 0.05, 0.3, 0.4, '2018/2019', '2019-06-25 03:02:05', '2019-06-25 08:48:27'),
(16, 'PW1b', '999123456', 'SIM', '0', 1, 'Baru', '2', 0.15, 0.1, 0.05, 0.3, 0.4, '2018/2019', '2019-06-25 07:29:23', '2019-06-25 08:47:37'),
(18, 'M001', '999123456', 'SIM', '1', 0, 'Baru', '1', 0, 0, 0, 0, 0, '2017/2018', '2019-06-28 05:12:45', '2019-06-28 05:12:45'),
(19, 'M001', '999123456', 'SIM', '1', 0, 'Baru', '1', 0, 0, 0, 0, 0, '2017/2018', '2019-06-28 05:12:45', '2019-06-28 05:12:45');

-- --------------------------------------------------------

--
-- Table structure for table `leaders`
--

CREATE TABLE `leaders` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lecturers`
--

CREATE TABLE `lecturers` (
  `id` int(11) NOT NULL,
  `kode` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `nama` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `telpon` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `alamat` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `nidn` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `pend_s1` varchar(64) COLLATE latin1_general_ci NOT NULL,
  `pend_s2` varchar(64) COLLATE latin1_general_ci NOT NULL,
  `pend_s3` varchar(64) COLLATE latin1_general_ci NOT NULL,
  `foto` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `lecturers`
--

INSERT INTO `lecturers` (`id`, `kode`, `nama`, `telpon`, `email`, `alamat`, `nidn`, `pend_s1`, `pend_s2`, `pend_s3`, `foto`, `created_at`, `updated_at`) VALUES
(6, 'd002', 'Haryanto, SAG.', '086777543122', 'hr@bing.com', 'Jl. Kedokan Bunder, Kedokan Indramayu', '444123555', 'S1 Manajemen', 'Sistem Informasi', 'Sistem Informasi', 'haryanto1561428320.jpg', '2019-06-19 16:15:24', '2019-06-24 19:05:20'),
(5, 'd001', 'Nur Rohman, Spd.', '087666578444', 'nr@yahoo.com', 'Ds. Kali Wedi Susukan Cirebon', '333245123', 'Pendidikan Agama Islam', 'Pendidikan Agama Islam', '-', 'nur-rohman1560985958.jpg', '2019-06-19 16:12:38', '2019-06-19 16:12:38');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_06_08_034359_create_user_roles_table', 1),
(4, '2019_06_10_153456_create_user_menus_table', 1),
(5, '2019_06_10_153523_create_user_sub_menus_table', 1),
(6, '2019_06_10_153549_create_user_access_menus_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', '2019-06-16 17:00:00', '2019-06-16 17:00:00'),
(2, 'Admin', '2019-06-16 17:00:00', '2019-06-16 17:00:00'),
(3, 'Mahasiswa', '2019-06-16 17:00:00', '2019-06-16 17:00:00'),
(4, 'Dosen', '2019-06-16 17:00:00', '2019-06-16 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `kode` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nama` varchar(191) COLLATE latin1_general_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `kode`, `nama`, `created_at`, `updated_at`) VALUES
(2, 'R001', 'Ruangan meating', '2019-06-14 19:20:45', '2019-06-14 19:20:45'),
(3, 'B002', 'Ruangan Lantai Atas', '2019-06-14 23:55:55', '2019-06-14 23:55:55'),
(4, 'R00A1', 'Ruangan A, Lantai Atas', '2019-06-24 20:13:14', '2019-06-24 20:13:43'),
(5, 'B0061A', 'Ruangan B, Lantai Bawah', '2019-06-24 20:14:02', '2019-06-24 20:14:02');

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(9) NOT NULL,
  `kode_mk` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `kode_ruang` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `kode_jurusan` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `semester` int(1) NOT NULL,
  `hari` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `jam_mulai` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `jam_selesai` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `kode_dosen` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `kelas_id` int(1) DEFAULT NULL,
  `tahun_akademik` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `presensi` float DEFAULT NULL,
  `tugas` float DEFAULT NULL,
  `quiz` float DEFAULT NULL,
  `uts` float DEFAULT NULL,
  `uas` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `kode_mk`, `kode_ruang`, `kode_jurusan`, `semester`, `hari`, `jam_mulai`, `jam_selesai`, `kode_dosen`, `kelas_id`, `tahun_akademik`, `presensi`, `tugas`, `quiz`, `uts`, `uas`, `created_at`, `updated_at`) VALUES
(1, 'M001', 'R001', 'S1A', 1, 'Sabtu', '09:00', '10:00', 'd002', 1, '2018/2019', 0.15, 0.1, 0.05, 0.3, 0.4, '2019-06-14 23:44:04', '2019-06-22 17:54:12'),
(5, 'F001', 'B002', 'S1A', 1, 'Senin', '08:00', '09:00', 'd001', 1, '2018/2019', 0.15, 0.1, 0.05, 0.3, 0.4, '2019-06-14 23:44:04', '2019-06-23 12:53:09'),
(18, 'A0LGD', 'R001', 'SIM', 1, 'Rabu', '16:00', '18:00', 'd002', 2, '2018/2019', 0.15, 0.1, 0.05, 0.3, 0.4, '2019-06-25 02:32:02', '2019-06-25 03:02:39'),
(19, 'P00as', 'R00A1', 'SIM', 1, 'Kamis', '16:00', '18:00', 'd002', 2, '2018/2019', 0.15, 0.1, 0.05, 0.3, 0.4, '2019-06-25 03:02:05', '2019-06-25 07:32:26'),
(20, 'PW1b', 'R00A1', 'SIM', 1, 'Jum\'at', '16:00', '18:00', 'd002', 2, '2018/2019', 0.15, 0.1, 0.05, 0.3, 0.4, '2019-06-25 07:29:23', '2019-06-25 07:32:20');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `no` int(10) NOT NULL,
  `tahun_akad` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `nim` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `nama` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `tempat_lahir` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `tanggal_lahir` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `alamat` text COLLATE latin1_general_ci NOT NULL,
  `kode_jurusan` varchar(4) COLLATE latin1_general_ci NOT NULL,
  `semester` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `jenis_kelamin` enum('L','P') COLLATE latin1_general_ci NOT NULL,
  `email` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `telpon` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `foto` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `status_krs` varchar(2) COLLATE latin1_general_ci NOT NULL DEFAULT '1',
  `angkatan` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `kode_kuri` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `no_ijazah` varchar(64) COLLATE latin1_general_ci DEFAULT NULL,
  `judul_skripsi` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `krs` int(11) DEFAULT NULL,
  `khs` int(11) DEFAULT NULL,
  `kelas_id` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `nim`, `nama`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `kode_jurusan`, `semester`, `jenis_kelamin`, `email`, `telpon`, `foto`, `status_krs`, `angkatan`, `kode_kuri`, `no_ijazah`, `judul_skripsi`, `krs`, `khs`, `kelas_id`, `created_at`, `updated_at`) VALUES
(10, '234111789', 'Nursandi', 'Cirebon', '11/05/2001', 'Jl. Kibandang, Slangit Klangenan Cirebon', 'S1A', '1', 'L', 'nursandi@gmail.com', '081324779934', 'nursandi1561430577.jpg', '0', '2019', NULL, NULL, NULL, 1, 1, '1', '2019-06-19 07:23:20', '2019-06-24 19:42:57'),
(12, '234111678', 'Siti Rohmah', 'Cirebon', '06/06/2002', 'Gumulung Lebak, Greged Cirebon', 'S1A', '1', 'P', 'sr@gmail.com', '087666455321', 'siti-rohmah1561428291.jpg', '0', '2019', NULL, NULL, NULL, 0, 1, '1', '2019-06-19 07:40:12', '2019-06-25 16:19:23'),
(15, '999123456', 'Dalim', 'Cirebon', '06/25/1999', 'Jl. Kepanjen No. 57 Magelang Jawa Tengah', 'SIM', '1', 'L', 'dalim@gmail.com', '089789765234', 'dalim1561455968.jpg', '0', '2019', NULL, NULL, NULL, 0, 1, '2', '2019-06-24 23:46:41', '2019-06-25 07:06:05'),
(9, '2341234567', 'Romadhoni', 'Cirebon', '06/19/2000', 'Ds. Kebon Gedang, Ciwaringin Cirebon', 'S1A', '1', 'L', 'rdhoni@gmail.com', '089555777324', 'romadhoni1561430587.jpg', '1', '2019', NULL, NULL, NULL, 1, 1, '1', '2019-06-19 06:56:39', '2019-06-24 19:43:07');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(5) NOT NULL DEFAULT 3,
  `is_active` int(5) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `user_id`, `email`, `email_verified_at`, `password`, `image`, `role_id`, `is_active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'TU', '$2y$10$v38CO6FaWHNWk1WUht2TaeDHENpfQ2o1042QhdFKFzdxjkYqvyLge', 'superadmin@gmail.com', NULL, '$2y$10$v38CO6FaWHNWk1WUht2TaeDHENpfQ2o1042QhdFKFzdxjkYqvyLge', 'default.png', 1, 1, 'UnUe4EvBQTEpW6OoF0zj4WDuv0ZxsLpphNfuOhZeVcMIJAi7TdrXVyVhe3Dr', '2019-06-11 21:54:00', '2019-06-11 21:54:00'),
(2, 'Staf TU', '$2y$10$rmwWOn3Wki/xrIWoY3PU1OgmoXyraQbppisUAwRdFiINCMNbV43F.', 'admin@gmail.com', NULL, '$2y$10$rmwWOn3Wki/xrIWoY3PU1OgmoXyraQbppisUAwRdFiINCMNbV43F.', 'staf-tu1560953285.jpg', 2, 1, '8qMug35VxNN1nxHVDR3AbZ9VaDU0N9M7UThNktJ8KHJNuKdXF9LVnZeAxrmL', '2019-06-11 21:54:00', '2019-06-19 07:08:05'),
(7, 'Romadhoni', '2341234567', 'rdhoni@gmail.com', NULL, '$2y$10$5rarWtBCsnuBRzf1kRAKV.2kwOxawPAoqC7KlitjWDq0W0lj0HHVi', 'romadhoni1561430587.jpg', 3, 1, NULL, '2019-06-19 06:56:39', '2019-06-24 19:43:07'),
(8, 'Nursandi', '234111789', 'nursandi@gmail.com', NULL, '$2y$10$r0lOeT/uPrFpDwRNSzEzLuk4d2rbsbkObUW4Pn.7J60fF0R6LzQn2', 'nursandi1561430577.jpg', 3, 1, 'TEBWkIrGjejCatLJ4BXvUCWvPJLEO5iAI9EeZKo565HGAdwdOy76BFm5C9DD', '2019-06-19 07:23:20', '2019-06-24 19:42:57'),
(10, 'Siti Rohmah', '234111678', 'sr@gmail.com', NULL, '$2y$10$7oISJtVnN2HJhTM5135zr.2SLpZaSv/AWt.56URJCqb1NB2lG2RkG', 'siti-rohmah1561428291.jpg', 3, 1, 'Bg02QrOYITKTToFmIWL5OT5bpIntIEdZZUGcOmdYUEmVd8vk46kRyrBFNlL2', '2019-06-19 07:40:12', '2019-06-24 19:04:51'),
(11, 'Nur Rohman', '333245123', 'nr@yahoo.com', NULL, '$2y$10$l/eYGLFDtna1.Y1208u2DOc5CYhg401ySBZktbLuNQO3h9AWJTOx2', 'nur-rohman1560985958.jpg', 4, 1, NULL, '2019-06-19 16:12:38', '2019-06-23 12:52:03'),
(12, 'Haryanto', '444123555', 'hr@bing.com', NULL, '$2y$10$vHR8bloxi33Vke9OdfnCn.vLv0c2mYBB91Vn8fVE8KLDV7s13a136', 'haryanto1561428320.jpg', 4, 1, '6fzMRIAJ2N6qGHJ7LTe1bAQ0GIrT4R9aKM80vkyz16EMfNk1ckGEcbfUOmnY', '2019-06-19 16:15:24', '2019-06-24 19:05:20'),
(15, 'Dalim', '999123456', 'dalim@gmail.com', NULL, '$2y$10$uFlaMZm0UYT5DWS/2dsJpuSxa9ugHY.rwNStCw5DY4AbsUzY08GJm', 'dalim1561455968.jpg', 3, 1, 'LJhEiJuA3dKeLETj6NV1WW0pNSW0T9bDezHRHFuJ8OPwlEvYnk4Y3OsqLQgK', '2019-06-24 23:46:41', '2019-06-25 02:46:08');

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menus`
--

CREATE TABLE `user_access_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` smallint(5) UNSIGNED NOT NULL,
  `menu_id` smallint(5) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_access_menus`
--

INSERT INTO `user_access_menus` (`id`, `role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2019-06-11 17:00:00', '2019-06-11 17:00:00'),
(2, 1, 2, '2019-06-11 17:00:00', '2019-06-11 17:00:00'),
(3, 2, 2, '2019-06-11 17:00:00', '2019-06-11 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_menus`
--

CREATE TABLE `user_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_menus`
--

INSERT INTO `user_menus` (`id`, `menu`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '2019-06-11 17:00:00', '2019-06-11 17:00:00'),
(2, 'Member', '2019-06-11 17:00:00', '2019-06-11 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_sub_menus`
--

CREATE TABLE `user_sub_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` smallint(5) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_sub_menus`
--

INSERT INTO `user_sub_menus` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, 'Dashboard', '/', 'fas fa-tachometer-alt', 1, '2019-06-11 17:00:00', '2019-06-11 17:00:00'),
(2, 1, 'Mahasiswa', '/students', 'fas fa-user-graduate', 1, '2019-06-11 17:00:00', '2019-06-11 17:00:00'),
(3, 1, 'Kurikulum', '/curriculums', 'far fa-newspaper', 1, '2019-06-11 17:00:00', '2019-06-11 17:00:00'),
(4, 1, 'Dosen', '/lecturers', 'fas fa-users', 1, '2019-06-11 17:00:00', '2019-06-11 17:00:00'),
(5, 1, 'Jurusan', '/departments', 'fas fa-chalkboard', 1, '2019-06-11 17:00:00', '2019-06-11 17:00:00'),
(6, 1, 'Mata Kuliah', '/courses', 'fas fa-book', 1, '2019-06-11 17:00:00', '2019-06-11 17:00:00'),
(7, 1, 'Jadwal', '/schedules', 'fas fa-clipboard-list', 1, '2019-06-11 17:00:00', '2019-06-11 17:00:00'),
(9, 1, 'Nilai Mahasiswa', '/values', 'flaticon-interface-6', 1, '2019-06-11 17:00:00', '2019-06-11 17:00:00'),
(10, 1, 'Ijazah', '/Diplomas', 'fas fa-th-large', 1, '2019-06-11 17:00:00', '2019-06-11 17:00:00'),
(11, 1, 'Tahun Akademik', '/years', 'fas fa-calendar-alt', 1, '2019-06-11 17:00:00', '2019-06-11 17:00:00'),
(12, 1, 'Manajemen User', '/users', 'fas fa-users-cog', 1, '2019-06-11 17:00:00', '2019-06-11 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `years`
--

CREATE TABLE `years` (
  `id` int(5) NOT NULL,
  `tahun` varchar(15) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `years`
--

INSERT INTO `years` (`id`, `tahun`, `created_at`, `updated_at`) VALUES
(1, '2018/2019', '2019-06-14 17:00:00', '2019-06-14 17:00:00'),
(2, '2019/2020', '2019-06-14 17:00:00', '0000-00-00 00:00:00'),
(3, '2020/2021', '2019-06-15 00:42:46', '2019-06-15 00:45:48'),
(5, '2017/2018', '2019-06-21 04:51:53', '2019-06-21 04:51:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `class_times`
--
ALTER TABLE `class_times`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `curricula`
--
ALTER TABLE `curricula`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `khs`
--
ALTER TABLE `khs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `krs`
--
ALTER TABLE `krs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leaders`
--
ALTER TABLE `leaders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lecturers`
--
ALTER TABLE `lecturers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nim` (`nim`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_access_menus`
--
ALTER TABLE `user_access_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_menus`
--
ALTER TABLE `user_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sub_menus`
--
ALTER TABLE `user_sub_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `years`
--
ALTER TABLE `years`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `class_times`
--
ALTER TABLE `class_times`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `curricula`
--
ALTER TABLE `curricula`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `khs`
--
ALTER TABLE `khs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `krs`
--
ALTER TABLE `krs`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `leaders`
--
ALTER TABLE `leaders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `lecturers`
--
ALTER TABLE `lecturers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `no` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `user_access_menus`
--
ALTER TABLE `user_access_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_menus`
--
ALTER TABLE `user_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_sub_menus`
--
ALTER TABLE `user_sub_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `years`
--
ALTER TABLE `years`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
